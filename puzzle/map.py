import sys

WIDTH = 50
HEIGHT = 80

def parse(lines):
  m = [[-1 for i in range(WIDTH)] for j in range(HEIGHT)]
  assert len(lines) == HEIGHT
  for i in range(len(lines)):
    line = lines[i]
    tokens = line.strip().split()
    assert len(tokens) == WIDTH
    for j in range(len(tokens)):
      token = tokens[j]
      assert token[0] in ['a', 'b', 'A', 'B', 'n', 'v']
      m[i][j] = int(token[1:])
  return m

def parseNV(lines):
  m = [[-1 for i in range(WIDTH)] for j in range(HEIGHT)]
  assert len(lines) == HEIGHT
  for i in range(len(lines)):
    line = lines[i]
    tokens = line.strip().split()
    assert len(tokens) == WIDTH
    for j in range(len(tokens)):
      token = tokens[j]
      assert token[0] in ['a', 'b', 'A', 'B', 'n', 'v']
      m[i][j] = token[0]
  return m

def isEqualLines(m):
  for i in range(HEIGHT):
    for j in range(i + 1, HEIGHT):
      if m[i] == m[j]:
        print i

def isEqualCols(m):
  for i in range(WIDTH):
    for j in range(i + 1, WIDTH):
      equal = True
      for k in range(HEIGHT):
        if m[k][i] != m[k][j]:
	  equal = False
	  break
      if equal:
	print i

def zeroes(m):
  count = 0
  for i in range(HEIGHT):
    for j in range(WIDTH):
      if m[i][j] == 0:
        count += 1
  print count

def is416(m):
  for i in range(HEIGHT):
    for j in range(WIDTH):
      if '416' in str(m[i][j]):
	pass
        #print str(i) + ', ' + str(j) + ': ' + str(m[i][j])

def printM(m):
  s = ''
  for row in m:
    t = ''
    for el in row:
      t += str(el) + ' '
    t += '\n'
    s += t
  print s

def stats(m):
  mx = -1
  mn = 999999
  hist = {}
  for row in m:
    for el in row:
      if el > mx:
        mx = el
      if el < mn:
        mn = el
      if el not in hist:
        hist[el] = 0
      hist[el] += 1
  print 'max ', mx, ', min ', mn
  freqs = [[] for i in range(5)]
  for k in hist:
    freqs[hist[k]].append(k)
  print 'appearing 3 times = ', freqs[3]
  print 'appearing 4 times = ', freqs[4]
  return hist

def buckets(m, hist):
  bucket_size = 100
  for i in range(10000 / bucket_size):
    mn = max(1, bucket_size * i)
    mx = min(bucket_size * (i + 1) - 1, 9998)
    num_appearing = 0
    for j in range(mn, mx + 1):
      if j in hist:
        num_appearing += 1
    print '(', mn, ',', mx, ') = ', num_appearing

def main():
  lines = [line.strip() for line in sys.stdin.readlines()]
  m = parse(lines)
  #isEqualLines(m)
  #isEqualCols(m)
  #zeroes(m)
  #is416(m)
  #printM(m)
  #hist = stats(m)
  #buckets(m, hist)
  mm = parseNV(lines)
  print printM(mm)

if __name__ == '__main__':
  main()
