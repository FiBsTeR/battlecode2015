package idlebot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class RobotPlayer {

  public static void run(RobotController rc) {
    while (true) {
      try {
        if (rc.getType() == RobotType.HQ ||
            rc.getType() == RobotType.TOWER) {
          maybeAttack(rc);
        }
        rc.yield();
      } catch (GameActionException e) {

      }
    }
  }

  private static void maybeAttack(RobotController rc) throws GameActionException {
    if (!rc.isWeaponReady()) {
      return;
    }

    MapLocation myLoc = rc.getLocation();
    RobotInfo[] enemyRobots = rc.senseNearbyRobots(100, rc.getTeam().opponent());
    for (RobotInfo enemyRobot : enemyRobots) {
      if (myLoc.distanceSquaredTo(enemyRobot.location) <=
          rc.getType().attackRadiusSquared) {
        rc.attackLocation(enemyRobot.location);
        return;
      }
    }
  }
}
