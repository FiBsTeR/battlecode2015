package team102;

public interface BehaviorSerializer {

  public int serialize(Behavior behavior);

  public Behavior deserialize(int serializedBehavior);
}
