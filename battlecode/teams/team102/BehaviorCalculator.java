package team102;

import battlecode.common.GameActionException;

public interface BehaviorCalculator {

  public Behavior computeBehavior() throws GameActionException;
}
