package team102;

import team102.behaviors.MissileBehavior;
import team102.buildorders.MarioKartBuildOrder;
import battlecode.common.Clock;
import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class RobotPlayer {

  public static void run(RobotController rc) {
    // Special case missiles for bytecode efficiency.
    if (rc.getType() == RobotType.MISSILE) {
      while (true) {
        try {
          new MissileBehavior().execute(rc);
          rc.yield();
        } catch (GameActionException e) {
          System.out.println(e.getMessage());
        }
      }
    }

    RobotState robotState = new RobotState(
        new BugNavigationState(),
        new MiningState());
    BuildOrder buildOrder = new MarioKartBuildOrder();
    BehaviorCalculator behaviorCalculator = buildOrder.getBehaviorCalculator(rc, robotState);

    while (true) {
      try {
        int startRoundNum = Clock.getRoundNum();
        Behavior behavior = behaviorCalculator.computeBehavior();
        rc.setIndicatorString(0, "Behavior = " + behavior.getDescription());
        behavior.execute(rc);
        int endRoundNum = Clock.getRoundNum();
        if (endRoundNum > startRoundNum) {
          int numBytecodes = rc.getType().bytecodeLimit * (endRoundNum - startRoundNum) +
              Clock.getBytecodeNum();
          System.out.println("Exceeded bytecode limit: " + numBytecodes);
        }
        rc.yield();
      } catch (GameActionException e) {
        System.out.println(e.getMessage());
      }
    }
  }
}
