package team102;

import battlecode.common.GameActionException;

public class ConstantBehaviorCalculator implements BehaviorCalculator {

  private final Behavior behavior;

  public ConstantBehaviorCalculator(Behavior behavior) {
    this.behavior = behavior;
  }

  @Override
  public Behavior computeBehavior() throws GameActionException {
    return behavior;
  }

}
