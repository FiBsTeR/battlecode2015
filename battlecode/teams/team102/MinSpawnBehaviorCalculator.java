package team102;

import team102.behaviors.NullBehavior;
import team102.behaviors.SpawnBehavior;
import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class MinSpawnBehaviorCalculator implements BehaviorCalculator {

  private final BehaviorMessenger behaviorMessenger;
  private final RobotType spawnType;
  private final int minRobotsOfType;

  public MinSpawnBehaviorCalculator(
      RobotController rc,
      RobotState robotState,
      RobotType spawnType,
      int minRobotsOfType) {
    this.behaviorMessenger = new DefaultBehaviorMessenger(rc, robotState);
    this.spawnType = spawnType;
    this.minRobotsOfType = minRobotsOfType;
  }

  @Override
  public Behavior computeBehavior() throws GameActionException {
    int numRobotsOfType = behaviorMessenger.getNumAlliedRobots(spawnType);
    return numRobotsOfType < minRobotsOfType ?
        new SpawnBehavior(spawnType) :
        new NullBehavior();
  }

}
