package team102;

import team102.behaviors.NullBehavior;
import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public class MessengerBehaviorCalculator implements BehaviorCalculator {

  private final RobotController rc;
  private final BehaviorMessenger behaviorMessenger;

  public MessengerBehaviorCalculator(RobotController rc, RobotState robotState) {
    this.rc = rc;
    this.behaviorMessenger = new DefaultBehaviorMessenger(rc, robotState);
  }

  @Override
  public Behavior computeBehavior() throws GameActionException {
    Behavior behavior = behaviorMessenger.getBehavior(rc.getID());
    if (behavior != null) {
      return behavior;
    }

    return new NullBehavior();
  }
}
