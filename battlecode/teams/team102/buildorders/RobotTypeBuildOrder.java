package team102.buildorders;

import team102.Behavior;
import team102.BehaviorCalculator;
import team102.BehaviorMessenger;
import team102.BuildOrder;
import team102.ConstantBehaviorCalculator;
import team102.MessengerBehaviorCalculator;
import team102.RobotState;
import team102.behaviors.HqBuildOrderBehavior;
import team102.behaviors.MineBehavior;
import team102.behaviors.NullBehavior;
import team102.behaviors.StaticDefenseBehavior;
import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;

public abstract class RobotTypeBuildOrder implements BuildOrder {

  @Override
  public BehaviorCalculator getBehaviorCalculator(RobotController rc, RobotState robotState) {
    switch (rc.getType()) {
      // The most common types should be placed at the top for performance.
      case MINER:
        return miner(rc, robotState);
      case MINERFACTORY:
        return minerFactory(rc, robotState);
      case BEAVER:
        return beaver(rc, robotState);

      case AEROSPACELAB:
        return aerospaceLab(rc, robotState);
      case BARRACKS:
        return barracks(rc, robotState);
      case BASHER:
        return basher(rc, robotState);
      case COMMANDER:
        return commander(rc, robotState);
      case COMPUTER:
        return computer(rc, robotState);
      case DRONE:
        return drone(rc, robotState);
      case HANDWASHSTATION:
        return handwashStation(rc, robotState);
      case HELIPAD:
        return helipad(rc, robotState);
      case HQ:
        return hq(rc, robotState);
      case LAUNCHER:
        return launcher(rc, robotState);
      case MISSILE:
        return missile(rc, robotState);
      case SOLDIER:
        return soldier(rc, robotState);
      case SUPPLYDEPOT:
        return supplyDepot(rc, robotState);
      case TANK:
        return tank(rc, robotState);
      case TANKFACTORY:
        return tankFactory(rc, robotState);
      case TECHNOLOGYINSTITUTE:
        return technologyInstitute(rc, robotState);
      case TOWER:
        return tower(rc, robotState);
      case TRAININGFIELD:
        return trainingField(rc, robotState);
      default:
        return new ConstantBehaviorCalculator(new NullBehavior());
    }
  }

  @Override
  public void initializeForRound(
      RobotController rc, BehaviorMessenger behaviorMessenger) throws GameActionException {
    initializeInternal(rc, behaviorMessenger);
  }

  @Override
  public Behavior getBehaviorForRobot(RobotInfo robotInfo) throws GameActionException {
    switch (robotInfo.type) {
      // The most common types should be placed at the top for performance.
      case MINER:
        return miner(robotInfo);
      case MINERFACTORY:
        return minerFactory(robotInfo);
      case BEAVER:
        return beaver(robotInfo);

      case AEROSPACELAB:
        return aerospaceLab(robotInfo);
      case BARRACKS:
        return barracks(robotInfo);
      case BASHER:
        return basher(robotInfo);
      case COMMANDER:
        return commander(robotInfo);
      case COMPUTER:
        return computer(robotInfo);
      case DRONE:
        return drone(robotInfo);
      case HANDWASHSTATION:
        return handwashStation(robotInfo);
      case HELIPAD:
        return helipad(robotInfo);
      case HQ:
        return hq(robotInfo);
      case LAUNCHER:
        return launcher(robotInfo);
      case MISSILE:
        return missile(robotInfo);
      case SOLDIER:
        return soldier(robotInfo);
      case SUPPLYDEPOT:
        return supplyDepot(robotInfo);
      case TANK:
        return tank(robotInfo);
      case TANKFACTORY:
        return tankFactory(robotInfo);
      case TECHNOLOGYINSTITUTE:
        return technologyInstitute(robotInfo);
      case TOWER:
        return tower(robotInfo);
      case TRAININGFIELD:
        return trainingField(robotInfo);
      default:
        return null;
    }
  }

  protected abstract void initializeInternal(
      RobotController rc,
      BehaviorMessenger behaviorMessenger) throws GameActionException;

  protected BehaviorCalculator aerospaceLab(RobotController rc, RobotState robotState) {
    return getMessengerBehaviorCalculator(rc, robotState);
  }
  protected BehaviorCalculator barracks(RobotController rc, RobotState robotState) {
    return getMessengerBehaviorCalculator(rc, robotState);
  }
  protected BehaviorCalculator basher(RobotController rc, RobotState robotState) {
    return getMessengerBehaviorCalculator(rc, robotState);
  }
  protected BehaviorCalculator beaver(RobotController rc, RobotState robotState) {
    return getMessengerBehaviorCalculator(rc, robotState);
  }
  protected BehaviorCalculator commander(RobotController rc, RobotState robotState) {
    return getMessengerBehaviorCalculator(rc, robotState);
  }
  protected BehaviorCalculator computer(RobotController rc, RobotState robotState) {
    return getMessengerBehaviorCalculator(rc, robotState);
  }
  protected BehaviorCalculator drone(RobotController rc, RobotState robotState) {
    return getMessengerBehaviorCalculator(rc, robotState);
  }
  protected BehaviorCalculator handwashStation(RobotController rc, RobotState robotState) {
    return getMessengerBehaviorCalculator(rc, robotState);
  }
  protected BehaviorCalculator helipad(RobotController rc, RobotState robotState) {
    return getMessengerBehaviorCalculator(rc, robotState);
  }
  protected BehaviorCalculator hq(RobotController rc, RobotState robotState) {
    // By default, HQ emits behaviors for all other robots.
    return new ConstantBehaviorCalculator(
        new HqBuildOrderBehavior(this /* buildOrder */, robotState));
  }
  protected BehaviorCalculator launcher(RobotController rc, RobotState robotState) {
    return getMessengerBehaviorCalculator(rc, robotState);
  }
  protected BehaviorCalculator miner(RobotController rc, RobotState robotState) {
    // By default, miner mines.
    Behavior mineBehavior =
        new MineBehavior(robotState.getMiningState(), robotState.getBugNavigationState());
    return new ConstantBehaviorCalculator(mineBehavior);
  }
  protected BehaviorCalculator minerFactory(RobotController rc, RobotState robotState) {
    return getMessengerBehaviorCalculator(rc, robotState);
  }
  protected BehaviorCalculator missile(RobotController rc, RobotState robotState) {
    return getMessengerBehaviorCalculator(rc, robotState);
  }
  protected BehaviorCalculator soldier(RobotController rc, RobotState robotState) {
    return getMessengerBehaviorCalculator(rc, robotState);
  }
  protected BehaviorCalculator supplyDepot(RobotController rc, RobotState robotState) {
    return getMessengerBehaviorCalculator(rc, robotState);
  }
  protected BehaviorCalculator tank(RobotController rc, RobotState robotState) {
    return getMessengerBehaviorCalculator(rc, robotState);
  }
  protected BehaviorCalculator tankFactory(RobotController rc, RobotState robotState) {
    return getMessengerBehaviorCalculator(rc, robotState);
  }
  protected BehaviorCalculator technologyInstitute(RobotController rc, RobotState robotState) {
    return getMessengerBehaviorCalculator(rc, robotState);
  }
  protected BehaviorCalculator tower(RobotController rc, RobotState robotState) {
    // By default, tower shoots stuff.
    return new ConstantBehaviorCalculator(new StaticDefenseBehavior());
  }
  protected BehaviorCalculator trainingField(RobotController rc, RobotState robotState) {
    return getMessengerBehaviorCalculator(rc, robotState);
  }

  private BehaviorCalculator getMessengerBehaviorCalculator(
      RobotController rc, RobotState robotState) {
    return new MessengerBehaviorCalculator(rc, robotState);
  }

  protected Behavior aerospaceLab(RobotInfo robotInfo) throws GameActionException {
    return null;
  }
  protected Behavior barracks(RobotInfo robotInfo) throws GameActionException {
    return null;
  }
  protected Behavior basher(RobotInfo robotInfo) throws GameActionException {
    return null;
  }
  protected Behavior beaver(RobotInfo robotInfo) throws GameActionException {
    return null;
  }
  protected Behavior commander(RobotInfo robotInfo) throws GameActionException {
    return null;
  }
  protected Behavior computer(RobotInfo robotInfo) throws GameActionException {
    return null;
  }
  protected Behavior drone(RobotInfo robotInfo) throws GameActionException {
    return null;
  }
  protected Behavior handwashStation(RobotInfo robotInfo) throws GameActionException {
    return null;
  }
  protected Behavior helipad(RobotInfo robotInfo) throws GameActionException {
    return null;
  }
  protected Behavior hq(RobotInfo robotInfo) throws GameActionException {
    return null;
  }
  protected Behavior launcher(RobotInfo robotInfo) throws GameActionException {
    return null;
  }
  protected Behavior miner(RobotInfo robotInfo) throws GameActionException {
    return null;
  }
  protected Behavior minerFactory(RobotInfo robotInfo) throws GameActionException {
    return null;
  }
  protected Behavior missile(RobotInfo robotInfo) throws GameActionException {
    return null;
  }
  protected Behavior soldier(RobotInfo robotInfo) throws GameActionException {
    return null;
  }
  protected Behavior supplyDepot(RobotInfo robotInfo) throws GameActionException {
    return null;
  }
  protected Behavior tank(RobotInfo robotInfo) throws GameActionException {
    return null;
  }
  protected Behavior tankFactory(RobotInfo robotInfo) throws GameActionException {
    return null;
  }
  protected Behavior technologyInstitute(RobotInfo robotInfo) throws GameActionException {
    return null;
  }
  protected Behavior tower(RobotInfo robotInfo) throws GameActionException {
    return null;
  }
  protected Behavior trainingField(RobotInfo robotInfo) throws GameActionException {
    return null;
  }
}
