package team102.buildorders;

import team102.Behavior;
import team102.BehaviorCalculator;
import team102.BehaviorMessenger;
import team102.BuildOrder;
import team102.ConstantBehaviorCalculator;
import team102.DefaultBehaviorMessenger;
import team102.RobotState;
import team102.behaviors.HqBuildOrderBehavior;
import team102.behaviors.MarioKartBehavior;
import team102.behaviors.MissileBehavior;
import team102.behaviors.NullBehavior;
import team102.behaviors.ProxyAerospaceLabBehavior;
import team102.behaviors.ProxyLauncherBehavior;
import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;

public class MarioKartBuildOrder implements BuildOrder {

  @Override
  public BehaviorCalculator getBehaviorCalculator(RobotController rc,
      RobotState robotState) {
    switch (rc.getType()) {
      case AEROSPACELAB:
        return new ConstantBehaviorCalculator(new ProxyAerospaceLabBehavior());
      case BEAVER:
        BehaviorMessenger behaviorMessenger = new DefaultBehaviorMessenger(rc, robotState);
        Behavior behavior = new MarioKartBehavior(
            behaviorMessenger, robotState.getBugNavigationState());
        return new ConstantBehaviorCalculator(behavior);
      case HQ:
        return new ConstantBehaviorCalculator(
            new HqBuildOrderBehavior(this /* buildOrder */, robotState));
      case LAUNCHER:
        return new ConstantBehaviorCalculator(
            new ProxyLauncherBehavior(robotState.getBugNavigationState()));
      case MISSILE:
        return new ConstantBehaviorCalculator(new MissileBehavior());
      default:
        return new ConstantBehaviorCalculator(new NullBehavior());
    }
  }

  @Override
  public void initializeForRound(RobotController rc,
      BehaviorMessenger behaviorMessenger) throws GameActionException {}

  @Override
  public Behavior getBehaviorForRobot(RobotInfo robotInfo)
      throws GameActionException {
    return null;
  }

}
