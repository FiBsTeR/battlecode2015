package team102.buildorders;

import team102.Behavior;
import team102.BehaviorCalculator;
import team102.BehaviorMessenger;
import team102.BugNavigationState;
import team102.MessengerBehaviorCalculator;
import team102.MiningState;
import team102.RobotState;
import team102.behaviors.BugNavigationBehavior;
import team102.behaviors.BuildBehavior;
import team102.behaviors.MineBehavior;
import team102.behaviors.NullBehavior;
import team102.behaviors.SpawnBehavior;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;


public class BugNavigationTestBuildOrder extends RobotTypeBuildOrder {

  private BehaviorMessenger behaviorMessenger;
  private int enemyHqOffsetX;
  private int enemyHqOffsetY;

  @Override
  protected void initializeInternal(RobotController rc,
      BehaviorMessenger behaviorMessenger) throws GameActionException {
    this.behaviorMessenger = behaviorMessenger;

    MapLocation myHq = rc.senseHQLocation();
    MapLocation enemyHq = rc.senseEnemyHQLocation();
    enemyHqOffsetX = enemyHq.x - myHq.x;
    enemyHqOffsetY = enemyHq.y - myHq.y;
  }

  @Override
  protected Behavior beaver(RobotInfo robotInfo) throws GameActionException {
    int numMinerFactories = behaviorMessenger.getNumAlliedRobots(RobotType.MINERFACTORY);
    return numMinerFactories < 1 ?
        new BuildBehavior(RobotType.MINERFACTORY, 0, 0) :
        new NullBehavior();
  }

  @Override
  protected Behavior minerFactory(RobotInfo robotInfo) throws GameActionException {
    int numMiners = behaviorMessenger.getNumAlliedRobots(RobotType.MINER);
    return numMiners < 10 ?
        new SpawnBehavior(RobotType.MINER) :
        new NullBehavior();
  }

  @Override
  protected BehaviorCalculator miner(RobotController rc, RobotState robotState) {
    return new MessengerBehaviorCalculator(rc, robotState);
  }

  @Override
  protected Behavior miner(RobotInfo robotInfo) throws GameActionException {
    int numMiners = behaviorMessenger.getNumAlliedRobots(RobotType.MINER);
    return numMiners < 10 ?
        new MineBehavior(new MiningState(), new BugNavigationState()) :
        new BugNavigationBehavior(enemyHqOffsetX, enemyHqOffsetY, new BugNavigationState());
  }

}
