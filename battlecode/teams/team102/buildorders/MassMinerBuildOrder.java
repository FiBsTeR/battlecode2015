package team102.buildorders;

import team102.Behavior;
import team102.BehaviorCalculator;
import team102.BehaviorMessenger;
import team102.MinSpawnBehaviorCalculator;
import team102.RobotState;
import team102.behaviors.BuildBehavior;
import team102.behaviors.NullBehavior;
import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;


public class MassMinerBuildOrder extends RobotTypeBuildOrder {

  private BehaviorMessenger behaviorMessenger;

  @Override
  protected void initializeInternal(RobotController rc,
      BehaviorMessenger behaviorMessenger) throws GameActionException {
    this.behaviorMessenger = behaviorMessenger;
  }

  @Override
  protected Behavior beaver(RobotInfo robotInfo) throws GameActionException {
    int numMinerFactories = behaviorMessenger.getNumAlliedRobots(RobotType.MINERFACTORY);
    return numMinerFactories < 1 ?
        new BuildBehavior(RobotType.MINERFACTORY, 0, 0) :
        new NullBehavior();
  }

  @Override
  protected BehaviorCalculator minerFactory(RobotController rc, RobotState robotState) {
    return new MinSpawnBehaviorCalculator(
        rc,
        robotState,
        RobotType.MINER /* spawnType */,
        20 /* minRobotsOfType */);
  }

}
