package team102;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public interface Behavior {

  public BehaviorType getBehaviorType();

  public String getDescription();

  public void execute(RobotController rc) throws GameActionException;
}
