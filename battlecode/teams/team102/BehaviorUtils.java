package team102;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class BehaviorUtils {

  public static boolean isSafeLocation(
      MapLocation loc,
      RobotInfo[] nearbyEnemyRobotInfos,
      MapLocation enemyHqLoc,
      MapLocation[] enemyTowerLocs) throws GameActionException {
    for (RobotInfo robotInfo : nearbyEnemyRobotInfos) {
      MapLocation enemyLoc = robotInfo.location;
      MapLocation threatLoc = canMoveSoon(robotInfo) ?
          enemyLoc.add(enemyLoc.directionTo(loc)) :
          enemyLoc;
      if (threatLoc.distanceSquaredTo(loc) <= robotInfo.type.attackRadiusSquared) {
        return false;
      }
    }
    int hqRange = enemyTowerLocs.length >= 2 ? 35 : 24;
    if (enemyHqLoc.distanceSquaredTo(loc) <= hqRange) {
      return false;
    }
    for (MapLocation enemyTowerLoc : enemyTowerLocs) {
      if (enemyTowerLoc.distanceSquaredTo(loc) <= RobotType.TOWER.attackRadiusSquared) {
        return false;
      }
    }
    return true;
  }

  private static boolean canMoveSoon(RobotInfo enemyRobotInfo) {
    return enemyRobotInfo.coreDelay < 2 &&
        (enemyRobotInfo.type == RobotType.BASHER ||
            enemyRobotInfo.type == RobotType.BEAVER ||
            enemyRobotInfo.type == RobotType.COMMANDER ||
            enemyRobotInfo.type == RobotType.DRONE ||
            enemyRobotInfo.type == RobotType.SOLDIER ||
            enemyRobotInfo.type == RobotType.TANK);
  }
}
