package team102;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotType;

/**
 * A class which uses the team radio channels as follows:
 *
 * - 0 - 399: even channels are robot IDs, odd channels are serialized robot
 *     behaviors for the preceding robot ID.
 * - 2000 - 2100: channel 2000 + robotOrdinalType is number of allied robots of
 *     that type
 */
public class DefaultBehaviorMessenger implements BehaviorMessenger {

  private static final int MAX_ROBOTS = 200;
  private static final int ROBOT_COUNT_CHANNEL = 2000;

  private final RobotController rc;
  private final BehaviorSerializer behaviorSerializer;

  public DefaultBehaviorMessenger(RobotController rc, RobotState robotState) {
    this.rc = rc;
    this.behaviorSerializer = new DefaultBehaviorSerializer(robotState);
  }

  @Override
  public void setBehaviors(int[] robotIds, Behavior[] behaviors) throws GameActionException {
    for (int i = 0; i < robotIds.length; i++) {
      if (behaviors[i] != null) {
        int message = behaviorSerializer.serialize(behaviors[i]);
        rc.broadcast(2 * i, robotIds[i]);
        rc.broadcast(2 * i + 1, message);
      }
    }
  }

  @Override
  public Behavior getBehavior(int robotId) throws GameActionException {
    for (int i = 0; i < MAX_ROBOTS; i++) {
      int value = rc.readBroadcast(2 * i);
      if (value == robotId) {
        int message = rc.readBroadcast(2 * i + 1);
        return behaviorSerializer.deserialize(message);
      }
    }
    return null;
  }

  @Override
  public void setNumAlliedRobots(
      RobotType robotType, int numAlliedRobots) throws GameActionException {
    rc.broadcast(ROBOT_COUNT_CHANNEL + robotType.ordinal(), numAlliedRobots);
  }

  @Override
  public int getNumAlliedRobots(RobotType robotType) throws GameActionException {
    return rc.readBroadcast(ROBOT_COUNT_CHANNEL + robotType.ordinal());
  }
}
