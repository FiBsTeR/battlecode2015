package team102;

import battlecode.common.MapLocation;

public class MiningState {

  private MapLocation oreTarget;

  public MapLocation getOreTarget() {
    return oreTarget;
  }

  public void setOreTarget(MapLocation oreTarget) {
    this.oreTarget = oreTarget;
  }

  public void clearOreTarget() {
    this.oreTarget = null;
  }
}
