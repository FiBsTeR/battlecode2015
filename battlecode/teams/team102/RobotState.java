package team102;

public class RobotState {

  private final BugNavigationState bugNavigationState;
  private final MiningState miningState;

  public RobotState(BugNavigationState bugNavigationState, MiningState miningState) {
    this.bugNavigationState = bugNavigationState;
    this.miningState = miningState;
  }

  public BugNavigationState getBugNavigationState() {
    return bugNavigationState;
  }

  public MiningState getMiningState() {
    return miningState;
  }
}
