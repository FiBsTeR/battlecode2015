package team102;

import team102.behaviors.BugNavigationBehavior;
import team102.behaviors.BuildBehavior;
import team102.behaviors.DroneHarassBehavior;
import team102.behaviors.FlyNavigationBehavior;
import team102.behaviors.MineBehavior;
import team102.behaviors.NullBehavior;
import team102.behaviors.SpawnBehavior;
import battlecode.common.RobotType;

public class DefaultBehaviorSerializer implements BehaviorSerializer {

  private static final int OFFSET_PADDING = 500;

  private final RobotState robotState;

  public DefaultBehaviorSerializer(RobotState robotState) {
    this.robotState = robotState;
  }

  @Override
  public int serialize(Behavior behavior) {
    // First bit is always 0. Next 5 bits are type num. Last 26 bits are args.
    BehaviorType behaviorType = behavior.getBehaviorType();
    int args = 0;
    int dx, dy;
    switch (behaviorType) {
      case BUG_NAVIGATION:
        BugNavigationBehavior bugNavigationBehavior = (BugNavigationBehavior) behavior;
        dx = OFFSET_PADDING + bugNavigationBehavior.getTargetOffsetX();
        dy = OFFSET_PADDING + bugNavigationBehavior.getTargetOffsetY();
        args = (dx << 10) + dy;
        break;

      case BUILD:
        BuildBehavior buildBehavior = (BuildBehavior) behavior;
        int robotTypeOrdinal = buildBehavior.getBuildType().ordinal();
        dx = OFFSET_PADDING + buildBehavior.getTargetOffsetX();
        dy = OFFSET_PADDING + buildBehavior.getTargetOffsetY();
        args = (robotTypeOrdinal << 20) + (dx << 10) + dy;
        break;

      case DRONE_HARASS:
        DroneHarassBehavior droneHarassBehavior = (DroneHarassBehavior) behavior;
        dx = OFFSET_PADDING + droneHarassBehavior.getTargetOffsetX();
        dy = OFFSET_PADDING + droneHarassBehavior.getTargetOffsetY();
        args = (dx << 10) + dy;
        break;

      case FLY_NAVIGATION:
        FlyNavigationBehavior rallyBehavior = (FlyNavigationBehavior) behavior;
        dx = OFFSET_PADDING + rallyBehavior.getTargetOffsetX();
        dy = OFFSET_PADDING + rallyBehavior.getTargetOffsetY();
        args = (dx << 10) + dy;
        break;

      case MINE:
        args = 0;
        break;

      case SPAWN:
        SpawnBehavior spawnBehavior = (SpawnBehavior) behavior;
        int spawnTypeOrdinal = spawnBehavior.getSpawnType().ordinal();
        args = spawnTypeOrdinal;
        break;

      default:
        args = 0;
        break;
    }

    int behaviorTypeOrdinal = behaviorType.ordinal();
    return (behaviorTypeOrdinal << 26) + args;
  }

  @Override
  public Behavior deserialize(int serializedBehavior) {
    int behaviorTypeOrdinal = serializedBehavior >> 26;
    BehaviorType behaviorType = BehaviorType.values()[behaviorTypeOrdinal];
    int args = serializedBehavior % (1 << 26);
    int dx, dy;
    switch (behaviorType) {
      case BUG_NAVIGATION:
        dx = args >> 10;
        dy = args % (1 << 10);
        BugNavigationState bugNavigationState = robotState.getBugNavigationState();
        return new BugNavigationBehavior(
            dx - OFFSET_PADDING, dy - OFFSET_PADDING, bugNavigationState);

      case BUILD:
        RobotType buildType = RobotType.values()[args >> 20];
        dx = (args >> 10) % (1 << 10);
        dy = args % (1 << 10);
        return new BuildBehavior(buildType, dx - OFFSET_PADDING, dy - OFFSET_PADDING);

      case DRONE_HARASS:
        dx = args >> 10;
        dy = args % (1 << 10);
        return new DroneHarassBehavior(dx - OFFSET_PADDING, dy - OFFSET_PADDING);

      case FLY_NAVIGATION:
        dx = args >> 10;
        dy = args % (1 << 10);
        return new FlyNavigationBehavior(dx - OFFSET_PADDING, dy - OFFSET_PADDING);

      case MINE:
        return new MineBehavior(robotState.getMiningState(), robotState.getBugNavigationState());

      case SPAWN:
        RobotType spawnType = RobotType.values()[args];
        return new SpawnBehavior(spawnType);

      default:
        return new NullBehavior();
    }
  }
}
