package team102;

import battlecode.common.GameActionException;
import battlecode.common.RobotType;

public interface BehaviorMessenger {

  public void setBehaviors(int[] robotIds, Behavior[] behaviors) throws GameActionException;

  public Behavior getBehavior(int robotId) throws GameActionException;

  public void setNumAlliedRobots(
      RobotType robotType, int numAlliedRobots) throws GameActionException;

  public int getNumAlliedRobots(RobotType robotType) throws GameActionException;
}
