package team102.behaviors;

import team102.Behavior;
import team102.BehaviorType;
import team102.BugNavigationState;
import battlecode.common.Clock;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class ProxyLauncherBehavior implements Behavior {

  private final BugNavigationState bugNavigationState;

  public ProxyLauncherBehavior(BugNavigationState bugNavigationState) {
    this.bugNavigationState = bugNavigationState;
  }

  @Override
  public BehaviorType getBehaviorType() {
    return BehaviorType.PROXY_LAUNCHER;
  }

  @Override
  public String getDescription() {
    return "Proxy launcher";
  }

  @Override
  public void execute(RobotController rc) throws GameActionException {
    if (!rc.isCoreReady() || !rc.isWeaponReady()) {
      return;
    }

    int roundNum = Clock.getRoundNum();
    MapLocation myLoc = rc.getLocation();
    MapLocation myHq = rc.senseHQLocation();
    MapLocation target = computeTarget(rc);
    int distance = Math.max(Math.abs(myLoc.x - target.x), Math.abs(myLoc.y - target.y));
    rc.setIndicatorString(1, roundNum + ": Steps from target = " + distance);
    if (distance <= 5) {
      maybeLaunchMissiles(rc, target);
      return;
    }

    if (distance > 8) {
      RobotInfo[] alliedRobots = rc.senseNearbyRobots(8, rc.getTeam());
      for (RobotInfo ally : alliedRobots) {
        if (ally.type == RobotType.BEAVER){
          rc.disintegrate();
          return;
        }
      }
    }

    if (bugNavigationState.getStuckTurns() > 10) {
      rc.disintegrate();
      return;
    }

    int offsetX = target.x - myHq.x;
    int offsetY = target.y - myHq.y;
    new BugNavigationBehavior(offsetX, offsetY, bugNavigationState).execute(rc);
  }

  private MapLocation computeTarget(RobotController rc) {
    MapLocation myLoc = rc.getLocation();
    MapLocation target = rc.senseEnemyHQLocation();
    int distanceToTarget = myLoc.distanceSquaredTo(target);

    MapLocation[] enemyTowerLocs = rc.senseEnemyTowerLocations();
    for (MapLocation enemyTowerLoc : enemyTowerLocs) {
      int dist = myLoc.distanceSquaredTo(enemyTowerLoc);
      if (dist < distanceToTarget) {
        target = enemyTowerLoc;
        distanceToTarget = dist;
      }
    }

    return target;
  }

  private void maybeLaunchMissiles(RobotController rc, MapLocation target) throws GameActionException {
    if (rc.getMissileCount() < 3) {
      return;
    }

    Direction targetDir = rc.getLocation().directionTo(target);
    Direction[] targetDirs = new Direction[] {
      targetDir,
      targetDir.rotateLeft(),
      targetDir.rotateRight(),
      targetDir.rotateLeft().rotateLeft(),
      targetDir.rotateRight().rotateRight()
    };
    for (int i = 0; i < targetDirs.length; i++) {
      Direction d = targetDirs[i];
      if (rc.getMissileCount() > 0 && rc.canLaunch(d)) {
        rc.launchMissile(d);
      }
    }
  }

}
