package team102.behaviors;

import team102.Behavior;
import team102.BehaviorMessenger;
import team102.BehaviorType;
import team102.BuildOrder;
import team102.DefaultBehaviorMessenger;
import team102.RobotState;
import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class HqBuildOrderBehavior implements Behavior {

  private final BuildOrder buildOrder;
  private final RobotState robotState;

  public HqBuildOrderBehavior(BuildOrder buildOrder, RobotState robotState) {
    this.buildOrder = buildOrder;
    this.robotState = robotState;
  }

  @Override
  public BehaviorType getBehaviorType() {
    return BehaviorType.HQ_DEFAULT;
  }

  @Override
  public String getDescription() {
    return "HQ default";
  }

  @Override
  public void execute(RobotController rc) throws GameActionException {
    BehaviorMessenger behaviorMessenger = new DefaultBehaviorMessenger(rc, robotState);
    RobotInfo[] alliedRobotInfos = rc.senseNearbyRobots(99999, rc.getTeam());

    setNumAlliedRobots(rc, alliedRobotInfos, behaviorMessenger);
    setBehaviors(rc, alliedRobotInfos, behaviorMessenger);
    new StaticDefenseBehavior().execute(rc);
    new TransferSupplyBehavior().execute(rc);
    spawnBeavers(rc, behaviorMessenger);
  }

  private void setBehaviors(
      RobotController rc,
      RobotInfo[] alliedRobotInfos,
      BehaviorMessenger behaviorMessenger) throws GameActionException {
    int[] robotIds = new int[alliedRobotInfos.length];
    Behavior[] behaviors = new Behavior[alliedRobotInfos.length];
    int currentBehaviorIndex = 0;

    buildOrder.initializeForRound(rc, behaviorMessenger);

    for (int i = 0; i < alliedRobotInfos.length; i++) {
      RobotInfo robotInfo = alliedRobotInfos[i];

      Behavior behavior = buildOrder.getBehaviorForRobot(robotInfo);
      if (behavior != null) {
        robotIds[currentBehaviorIndex] = robotInfo.ID;
        behaviors[currentBehaviorIndex] = behavior;
      }
      currentBehaviorIndex++;
    }

    behaviorMessenger.setBehaviors(robotIds, behaviors);
  }

  private void setNumAlliedRobots(
      RobotController rc,
      RobotInfo[] alliedRobotInfos,
      BehaviorMessenger behaviorMessenger) throws GameActionException {
    int numAerospaceLabs = 0;
    int numBeavers = 0;
    int numDrones = 0;
    int numHelipads = 0;
    int numMiners = 0;
    int numMinerFactories = 0;
    for (int i = 0; i < alliedRobotInfos.length; i++) {
      RobotInfo robotInfo = alliedRobotInfos[i];
      switch (robotInfo.type) {
        case AEROSPACELAB:
          numAerospaceLabs++;
          break;
        case BEAVER:
          numBeavers++;
          break;
        case DRONE:
          numDrones++;
          break;
        case HELIPAD:
          numHelipads++;
          break;
        case MINER:
          numMiners++;
          break;
        case MINERFACTORY:
          numMinerFactories++;
          break;
      }
    }

    behaviorMessenger.setNumAlliedRobots(RobotType.AEROSPACELAB, numAerospaceLabs);
    behaviorMessenger.setNumAlliedRobots(RobotType.BEAVER, numBeavers);
    behaviorMessenger.setNumAlliedRobots(RobotType.DRONE, numDrones);
    behaviorMessenger.setNumAlliedRobots(RobotType.HELIPAD, numHelipads);
    behaviorMessenger.setNumAlliedRobots(RobotType.MINER, numMiners);
    behaviorMessenger.setNumAlliedRobots(RobotType.MINERFACTORY, numMinerFactories);
  }

  private void spawnBeavers(
      RobotController rc,
      BehaviorMessenger behaviorMessenger) throws GameActionException {
    int numBeavers = behaviorMessenger.getNumAlliedRobots(RobotType.BEAVER);
    if (numBeavers >= 1) {
      return;
    }

    new SpawnBehavior(RobotType.BEAVER).execute(rc);
  }
}
