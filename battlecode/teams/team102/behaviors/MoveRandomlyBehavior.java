package team102.behaviors;

import team102.Behavior;
import team102.BehaviorType;
import team102.BehaviorUtils;
import battlecode.common.Clock;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;

public class MoveRandomlyBehavior implements Behavior {

  @Override
  public BehaviorType getBehaviorType() {
    return BehaviorType.MOVE_RANDOMLY;
  }

  @Override
  public String getDescription() {
    return "Move randomly";
  }

  @Override
  public void execute(RobotController rc) throws GameActionException {
    if (!rc.isCoreReady()) {
      return;
    }

    MapLocation myLoc = rc.getLocation();
    RobotInfo[] nearbyEnemyRobotInfos = rc.senseNearbyRobots(40, rc.getTeam().opponent());
    MapLocation enemyHqLoc = rc.senseEnemyHQLocation();
    MapLocation[] enemyTowerLocs = rc.senseEnemyTowerLocations();

    Direction d = getRandomDirection(rc.getID());
    for (int i = 0; i < 8; i++) {
      if (rc.canMove(d) &&
          BehaviorUtils.isSafeLocation(
              myLoc.add(d),
              nearbyEnemyRobotInfos,
              enemyHqLoc,
              enemyTowerLocs)) {
        rc.move(d);
        return;
      }
      d = d.rotateLeft();
    }
  }

  private Direction getRandomDirection(int id) {
    return Direction.values()[(Clock.getRoundNum() % 3000 * id % 3000) % 8];
  }

}
