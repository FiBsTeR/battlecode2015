package team102.behaviors;

import team102.Behavior;
import team102.BehaviorType;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;

public class StaticDefenseBehavior implements Behavior {

  @Override
  public BehaviorType getBehaviorType() {
    return BehaviorType.STATIC_DEFENSE;
  }

  @Override
  public String getDescription() {
    return "Static defense";
  }

  @Override
  public void execute(RobotController rc) throws GameActionException {
    if (!rc.isWeaponReady()) {
      return;
    }

    RobotInfo[] nearbyEnemyRobotInfos = rc.senseNearbyRobots(
        rc.getType().attackRadiusSquared, rc.getTeam().opponent());

    MapLocation minHealthLoc = null;
    double minHealth = 99999;
    for (RobotInfo enemyRobotInfo : nearbyEnemyRobotInfos) {
      if (enemyRobotInfo.health < minHealth) {
        minHealth = enemyRobotInfo.health;
        minHealthLoc = enemyRobotInfo.location;
      }
    }

    if (minHealthLoc != null) {
      rc.attackLocation(minHealthLoc);
    }
  }

}
