package team102.behaviors;

import team102.Behavior;
import team102.BehaviorType;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;

public class MissileBehavior implements Behavior {

  @Override
  public BehaviorType getBehaviorType() {
    return BehaviorType.MISSILE;
  }

  @Override
  public String getDescription() {
    return "Missile";
  }

  @Override
  public void execute(RobotController rc) throws GameActionException {
    if (!rc.isCoreReady()) {
      return;
    }
    MapLocation myLoc = rc.getLocation();
    MapLocation target = rc.senseEnemyHQLocation();
    int targetDist = myLoc.distanceSquaredTo(target);
    RobotInfo[] enemyInfos = rc.senseNearbyRobots(60, rc.getTeam().opponent());
    if (enemyInfos.length > 0) {
      MapLocation loc = enemyInfos[0].location;
      int dist = myLoc.distanceSquaredTo(loc);
      if (dist < targetDist) {
        targetDist = dist;
        target = loc;
      }
    }

    if (targetDist <= 2) {
      rc.explode();
      return;
    }

    Direction targetDir = myLoc.directionTo(target);
    if (rc.canMove(targetDir)) {
      rc.move(targetDir);
      return;
    }
    if (rc.canMove(targetDir.rotateLeft())) {
      rc.move(targetDir.rotateLeft());
      return;
    }
    if (rc.canMove(targetDir.rotateRight())) {
      rc.move(targetDir.rotateRight());
      return;
    }
  }

}
