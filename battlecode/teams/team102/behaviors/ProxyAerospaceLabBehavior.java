package team102.behaviors;

import team102.Behavior;
import team102.BehaviorType;
import team102.BehaviorUtils;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class ProxyAerospaceLabBehavior implements Behavior {

  private static final int MAX_LAUNCHERS_PER_TOWER = 3;

  @Override
  public BehaviorType getBehaviorType() {
    return BehaviorType.PROXY_AEROSPACE_LAB;
  }

  @Override
  public String getDescription() {
    return "Proxy aerospace lab";
  }

  @Override
  public void execute(RobotController rc) throws GameActionException {
    MapLocation target = computeTarget(rc);
    if (maybeSelfDestruct(rc, target)) {
      return;
    }

    if (!rc.isCoreReady() || rc.getTeamOre() < RobotType.LAUNCHER.oreCost) {
      return;
    }

    int numNearbyLaunchers = 0;
    RobotInfo[] nearbyAlliedRobots = rc.senseNearbyRobots(100, rc.getTeam());
    for (RobotInfo alliedRobot : nearbyAlliedRobots) {
      if (alliedRobot.type == RobotType.LAUNCHER) {
        numNearbyLaunchers++;
      }
    }
    if (numNearbyLaunchers >= MAX_LAUNCHERS_PER_TOWER) {
      return;
    }

    Direction targetDir = rc.getLocation().directionTo(target);
    Direction[] targetDirs = new Direction[] {
      targetDir,
      targetDir.rotateLeft(),
      targetDir.rotateRight(),
      targetDir.rotateLeft().rotateLeft(),
      targetDir.rotateRight().rotateRight(),
      targetDir.rotateLeft().rotateLeft().rotateLeft(),
      targetDir.rotateLeft().rotateLeft().rotateRight(),
      targetDir.opposite()
    };

    MapLocation myLoc = rc.getLocation();
    RobotInfo[] nearbyEnemyRobotInfos = new RobotInfo[] {};
    MapLocation enemyHqLoc = rc.senseEnemyHQLocation();
    MapLocation[] enemyTowerLocs = rc.senseEnemyTowerLocations();
    for (Direction d : targetDirs) {
      if (rc.canSpawn(d, RobotType.LAUNCHER) &&
          BehaviorUtils.isSafeLocation(
              myLoc.add(d), nearbyEnemyRobotInfos, enemyHqLoc, enemyTowerLocs)) {
        rc.spawn(d, RobotType.LAUNCHER);
        return;
      }
    }
  }

  private MapLocation computeTarget(RobotController rc) {
    MapLocation myLoc = rc.getLocation();
    MapLocation target = rc.senseEnemyHQLocation();
    int distanceToTarget = myLoc.distanceSquaredTo(target);

    MapLocation[] enemyTowerLocs = rc.senseEnemyTowerLocations();
    for (MapLocation enemyTowerLoc : enemyTowerLocs) {
      int dist = myLoc.distanceSquaredTo(enemyTowerLoc);
      if (dist < distanceToTarget) {
        target = enemyTowerLoc;
        distanceToTarget = dist;
      }
    }

    return target;
  }

  private boolean maybeSelfDestruct(RobotController rc, MapLocation target) {
    if (rc.getLocation().distanceSquaredTo(target) < 60) {
      return false;
    }
    RobotInfo[] alliedRobots = rc.senseNearbyRobots(8, rc.getTeam());
    for (RobotInfo ally : alliedRobots) {
      if (ally.type == RobotType.BEAVER){
        return true;
      }
    }
    rc.disintegrate();
    return true;
  }

}
