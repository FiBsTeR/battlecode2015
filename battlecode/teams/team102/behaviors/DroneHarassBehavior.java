package team102.behaviors;

import team102.Behavior;
import team102.BehaviorType;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class DroneHarassBehavior implements Behavior {

  private final int targetOffsetX;
  private final int targetOffsetY;

  public DroneHarassBehavior(int targetOffsetX, int targetOffsetY) {
    this.targetOffsetX = targetOffsetX;
    this.targetOffsetY = targetOffsetY;
  }

  public int getTargetOffsetX() {
    return targetOffsetX;
  }

  public int getTargetOffsetY() {
    return targetOffsetY;
  }

  @Override
  public BehaviorType getBehaviorType() {
    return BehaviorType.DRONE_HARASS;
  }

  @Override
  public String getDescription() {
    return "Drone harass to (" + targetOffsetX + ", " + targetOffsetY + ")";
  }

  @Override
  public void execute(RobotController rc) throws GameActionException {
    RobotInfo[] nearbyEnemyRobotInfos = rc.senseNearbyRobots(
        RobotType.DRONE.attackRadiusSquared, rc.getTeam().opponent());

    if (rc.isWeaponReady()) {
      attack(rc, nearbyEnemyRobotInfos);
    }
    new FlyNavigationBehavior(targetOffsetX, targetOffsetY).execute(rc);
  }

  private void attack(
      RobotController rc,
      RobotInfo[] nearbyEnemyRobotInfos) throws GameActionException {
    // Attack the enemy with the lowest health.
    MapLocation lowestHealthLoc = null;
    double lowestHealth = 99999;
    for (RobotInfo robotInfo : nearbyEnemyRobotInfos) {
      if (robotInfo.health < lowestHealth) {
        lowestHealth = robotInfo.health;
        lowestHealthLoc = robotInfo.location;
      }
    }

    if (lowestHealthLoc != null) {
      rc.attackLocation(lowestHealthLoc);
    }
  }
}
