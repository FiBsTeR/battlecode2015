package team102.behaviors;

import team102.Behavior;
import team102.BehaviorType;
import team102.BehaviorUtils;
import team102.BugNavigationState;
import battlecode.common.Clock;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class BugNavigationBehavior implements Behavior {

  private final int targetOffsetX;
  private final int targetOffsetY;
  private final BugNavigationState bugNavigationState;

  public BugNavigationBehavior(
      int targetOffsetX, int targetOffsetY, BugNavigationState bugNavigationState) {
    this.targetOffsetX = targetOffsetX;
    this.targetOffsetY = targetOffsetY;
    this.bugNavigationState = bugNavigationState;
  }

  public int getTargetOffsetX() {
    return targetOffsetX;
  }

  public int getTargetOffsetY() {
    return targetOffsetY;
  }

  @Override
  public BehaviorType getBehaviorType() {
    return BehaviorType.BUG_NAVIGATION;
  }

  @Override
  public String getDescription() {
    return "Bug to (" + targetOffsetX + ", " + targetOffsetY + ")";
  }

  @Override
  public void execute(RobotController rc) throws GameActionException {
    if (!rc.isCoreReady()) {
      return;
    }

    int roundNum = Clock.getRoundNum();
    RobotInfo[] nearbyEnemyRobotInfos = rc.senseNearbyRobots(40, rc.getTeam().opponent());
    MapLocation enemyHqLoc = rc.senseEnemyHQLocation();
    MapLocation[] enemyTowerLocs = rc.senseEnemyTowerLocations();

    MapLocation myLoc = rc.getLocation();
    MapLocation target = rc.senseHQLocation().add(targetOffsetX, targetOffsetY);
    Direction targetDir = myLoc.directionTo(target);
    Direction[] targetDirs = rc.getID() % 2 == 0 ?
        new Direction[] {targetDir, targetDir.rotateLeft(), targetDir.rotateRight()} :
        new Direction[] {targetDir, targetDir.rotateRight(), targetDir.rotateLeft()};

    if (!bugNavigationState.isTracing()) {
      // If not tracing, try moving towards the target.
      for (Direction d : targetDirs) {
        if (rc.canMove(d) &&
            BehaviorUtils.isSafeLocation(
                myLoc.add(d),
                nearbyEnemyRobotInfos,
                enemyHqLoc,
                enemyTowerLocs)) {
          rc.move(d);
          rc.setIndicatorString(2, roundNum + ": Not tracing, moving towards target.");
          return;
        }
      }

      if (isAlliedRobotAtLoc(rc, myLoc.add(targetDir))) {
        rc.setIndicatorString(2, roundNum + ": Not tracing, waiting for ally.");
        bugNavigationState.stuck();
        return;
      }

      // Otherwise, start tracing.
      bugNavigationState.startTrace(myLoc.distanceSquaredTo(target), targetDir);
    }

    // Check if we should stop tracing.
    int currentDistance = myLoc.distanceSquaredTo(target);
    if (currentDistance < bugNavigationState.getTraceDistance() &&
        rc.canMove(targetDir) &&
        BehaviorUtils.isSafeLocation(
            myLoc.add(targetDir),
            nearbyEnemyRobotInfos,
            enemyHqLoc,
            enemyTowerLocs)) {
      rc.move(targetDir);
      bugNavigationState.endTrace();
      rc.setIndicatorString(2, roundNum + ": Stopped tracing, moving towards target.");
      return;
    }

    // Continue tracing.
    Direction nextStep = bugNavigationState.getNextStepDirection();
    Direction d = nextStep;
    for (int i = 0; i < 8; i++) {
      if (rc.canMove(d) &&
          BehaviorUtils.isSafeLocation(
              myLoc.add(d),
              nearbyEnemyRobotInfos,
              enemyHqLoc,
              enemyTowerLocs)) {
        // Hug the wall.
        rc.move(d);
        bugNavigationState.trace(d);
        rc.setIndicatorString(2, roundNum + ": Tracing, hugging wall. Started " + nextStep
            + ", stepping " + d + ".");
        return;
      }
      if (isAlliedRobotAtLoc(rc, myLoc.add(d))) {
        // If we can't move and there's an allied unit in front, just wait.
        rc.setIndicatorString(2, roundNum + ": Tracing, waiting for ally at " + d + ".");
        bugNavigationState.stuck();
        return;
      }
      d = d.rotateLeft();
    }

    rc.setIndicatorString(2, roundNum + ": I\'m stuck!.");
    bugNavigationState.stuck();
  }

  private boolean isAlliedRobotAtLoc(
      RobotController rc, MapLocation loc) throws GameActionException {
    RobotInfo robotInfo = rc.senseRobotAtLocation(loc);
    if (robotInfo == null || robotInfo.team != rc.getTeam()) {
      return false;
    }
    return robotInfo.type == RobotType.BASHER ||
        robotInfo.type == RobotType.BEAVER ||
        robotInfo.type == RobotType.COMMANDER ||
        robotInfo.type == RobotType.COMPUTER ||
        robotInfo.type == RobotType.DRONE ||
        robotInfo.type == RobotType.LAUNCHER ||
        robotInfo.type == RobotType.MINER ||
        robotInfo.type == RobotType.MISSILE ||
        robotInfo.type == RobotType.SOLDIER ||
        robotInfo.type == RobotType.TANK;
  }

}
