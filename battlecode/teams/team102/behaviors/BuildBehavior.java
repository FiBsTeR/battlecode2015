package team102.behaviors;

import team102.Behavior;
import team102.BehaviorType;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class BuildBehavior implements Behavior {

  private final RobotType buildType;
  private final int targetOffsetX;
  private final int targetOffsetY;

  public BuildBehavior(RobotType buildType, int targetOffsetX, int targetOffsetY) {
    this.buildType = buildType;
    this.targetOffsetX = targetOffsetX;
    this.targetOffsetY = targetOffsetY;
  }

  public RobotType getBuildType() {
    return buildType;
  }

  public int getTargetOffsetX() {
    return targetOffsetX;
  }

  public int getTargetOffsetY() {
    return targetOffsetY;
  }

  @Override
  public BehaviorType getBehaviorType() {
    return BehaviorType.BUILD;
  }

  @Override
  public String getDescription() {
    return "Build " + buildType + " at offset (" + targetOffsetX + ", " + targetOffsetY + ")";
  }

  @Override
  public void execute(RobotController rc) throws GameActionException {
    if (!rc.isCoreReady()) {
      return;
    }
    MapLocation target = rc.senseHQLocation().add(targetOffsetX, targetOffsetY);

    // Build if we're close to the target, we have enough money, and there's a
    // suitable location.
    MapLocation myLoc = rc.getLocation();
    if (myLoc.distanceSquaredTo(target) <= 2 &&
        rc.getTeamOre() > buildType.oreCost) {
      for (Direction d : Direction.values()) {
        if (rc.canBuild(d, buildType) && shouldBuild(myLoc.add(d))) {
          rc.build(d, buildType);
          return;
        }
      }
    }

    // Otherwise, move closer.
    new FlyNavigationBehavior(targetOffsetX, targetOffsetY).execute(rc);
  }

  private boolean shouldBuild(MapLocation loc) {
    return (loc.x + loc.y) % 2 == 0;
  }
}
