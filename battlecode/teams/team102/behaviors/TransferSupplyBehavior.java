package team102.behaviors;

import team102.Behavior;
import team102.BehaviorType;
import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;

public class TransferSupplyBehavior implements Behavior {

  @Override
  public BehaviorType getBehaviorType() {
    return BehaviorType.TRANSFER_SUPPLY;
  }

  @Override
  public String getDescription() {
    return "Transfer supply";
  }

  @Override
  public void execute(RobotController rc) throws GameActionException {
    RobotInfo[] nearbyRobotInfos = rc.senseNearbyRobots(
        GameConstants.SUPPLY_TRANSFER_RADIUS_SQUARED, rc.getTeam());
    double currentSupply = rc.getSupplyLevel();
    for (RobotInfo robotInfo : nearbyRobotInfos) {
      if (currentSupply < 1) {
        return;
      }

      double minSupply;
      switch (robotInfo.type) {
        case BEAVER:
          minSupply = 100000.0;
          break;
        default:
          minSupply = 0.0;
          break;
      }

      double diff = minSupply - robotInfo.supplyLevel;
      if (diff > 0) {
        int supplyToGive = (int) (currentSupply > diff ?
            diff :
            currentSupply);
        rc.transferSupplies(supplyToGive, robotInfo.location);
        currentSupply -= supplyToGive;
      }
    }
  }

}
