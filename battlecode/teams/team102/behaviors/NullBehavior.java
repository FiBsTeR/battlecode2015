package team102.behaviors;

import team102.Behavior;
import team102.BehaviorType;
import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public class NullBehavior implements Behavior {

  @Override
  public BehaviorType getBehaviorType() {
    return BehaviorType.NULL;
  }

  @Override
  public String getDescription() {
    return "Null";
  }

  @Override
  public void execute(RobotController rc) throws GameActionException {}

}
