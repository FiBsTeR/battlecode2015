package team102.behaviors;

import team102.Behavior;
import team102.BehaviorType;
import team102.BehaviorUtils;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;

public class FlyNavigationBehavior implements Behavior {

  private final int targetOffsetX;
  private final int targetOffsetY;

  public FlyNavigationBehavior(int targetOffsetX, int targetOffsetY) {
    this.targetOffsetX = targetOffsetX;
    this.targetOffsetY = targetOffsetY;
  }

  public int getTargetOffsetX() {
    return targetOffsetX;
  }

  public int getTargetOffsetY() {
    return targetOffsetY;
  }

  @Override
  public BehaviorType getBehaviorType() {
    return BehaviorType.FLY_NAVIGATION;
  }

  @Override
  public String getDescription() {
    return "Fly to (" + targetOffsetX + ", " + targetOffsetY + ")";
  }

  @Override
  public void execute(RobotController rc) throws GameActionException {
    if (!rc.isCoreReady()) {
      return;
    }

    MapLocation target = rc.senseHQLocation().add(targetOffsetX, targetOffsetY);
    if (rc.getLocation().distanceSquaredTo(target) <= 2) {
      return;
    }

    RobotInfo[] nearbyEnemyRobotInfos = rc.senseNearbyRobots(
        rc.getType().attackRadiusSquared, rc.getTeam().opponent());
    MapLocation enemyHqLoc = rc.senseEnemyHQLocation();
    MapLocation[] enemyTowerLocs = rc.senseEnemyTowerLocations();

    MapLocation myLocation = rc.getLocation();
    Direction dir = myLocation.directionTo(target);
    boolean tryLeftFirst = rc.getID() % 2 == 0;
    Direction[] dirsToTry = tryLeftFirst ?
        new Direction[] {
          dir,
          dir.rotateLeft(),
          dir.rotateRight(),
          dir.rotateLeft().rotateLeft(),
          dir.rotateRight().rotateRight(),
          dir.opposite().rotateRight(),
          dir.opposite().rotateLeft(),
          dir.opposite()
        } :
        new Direction[] {
          dir,
          dir.rotateRight(),
          dir.rotateLeft(),
          dir.rotateRight().rotateRight(),
          dir.rotateLeft().rotateLeft(),
          dir.opposite().rotateLeft(),
          dir.opposite().rotateRight(),
          dir.opposite()
        };
    for (Direction d : dirsToTry) {
      if (rc.canMove(d) &&
          BehaviorUtils.isSafeLocation(
              myLocation.add(d),
              nearbyEnemyRobotInfos,
              enemyHqLoc,
              enemyTowerLocs)) {
        rc.move(d);
        return;
      }
    }
  }

}
