package team102.behaviors;

import team102.Behavior;
import team102.BehaviorType;
import team102.BehaviorUtils;
import team102.BugNavigationState;
import team102.MiningState;
import battlecode.common.Clock;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;

public class MineBehavior implements Behavior {

  private static final double MIN_ORE_TO_MINE = 2.0;
  private static final int MAX_SEARCH_DISTANCE = 8;

  private final MiningState miningState;
  private final BugNavigationState bugNavigationState;

  public MineBehavior(MiningState miningState, BugNavigationState bugNavigationState) {
    this.miningState = miningState;
    this.bugNavigationState = bugNavigationState;
  }

  @Override
  public BehaviorType getBehaviorType() {
    return BehaviorType.MINE;
  }

  @Override
  public String getDescription() {
    return "Mine";
  }

  @Override
  public void execute(RobotController rc) throws GameActionException {
    if (!rc.isCoreReady()) {
      return;
    }

    int roundNum = Clock.getRoundNum();
    MapLocation myLoc = rc.getLocation();
    double myOre = rc.senseOre(myLoc);
    if (myOre > MIN_ORE_TO_MINE) {
      // If there's a lot of ore, just mine here and clear any stored ore
      // location.
      rc.mine();
      miningState.clearOreTarget();
      rc.setIndicatorString(1, roundNum + ": Mining.");
      return;
    }

    // Move to the closest safe tile next to me with the most ore.
    Direction bestDirection = Direction.NONE;
    double mostOre = MIN_ORE_TO_MINE;

    RobotInfo[] nearbyEnemyRobotInfos = rc.senseNearbyRobots(40, rc.getTeam().opponent());
    MapLocation enemyHqLoc = rc.senseEnemyHQLocation();
    MapLocation[] enemyTowerLocs = rc.senseEnemyTowerLocations();
    for (Direction d : Direction.values()) {
      if (d == Direction.NONE || d == Direction.OMNI) {
        continue;
      }
      double ore = rc.senseOre(myLoc.add(d));
      if (rc.canMove(d) &&
          ore > mostOre &&
          BehaviorUtils.isSafeLocation(
              myLoc.add(d),
              nearbyEnemyRobotInfos,
              enemyHqLoc,
              enemyTowerLocs)) {
        mostOre = ore;
        bestDirection = d;
      }
    }

    if (bestDirection != Direction.NONE) {
      rc.move(bestDirection);
      rc.setIndicatorString(1, roundNum + ": Ore next to me, shimmying.");
      return;
    }

    // If there is no adjacent location with enough ore, compute a target
    // (or retrieve from state) and navigate to it.
    MapLocation oreTarget = miningState.getOreTarget();
    if (oreTarget != null && !isValidOreTarget(oreTarget, rc)) {
      // If the ore target is no longer valid, clear it.
      miningState.clearOreTarget();
      oreTarget = null;
    }
    if (oreTarget == null) {
      oreTarget = computeOreTarget(rc);
      miningState.setOreTarget(oreTarget);
    }
    if (oreTarget == null) {
      for (Direction d : Direction.values()) {
        if (rc.canMove(d)) {
          rc.move(d);
          rc.setIndicatorString(1, roundNum + ": No ore in sight, moving randomly.");
          return;
        }
      }
      return;
    }

    MapLocation myHq = rc.senseHQLocation();
    int oreTargetOffsetX = oreTarget.x - myHq.x;
    int oreTargetOffsetY = oreTarget.y - myHq.y;
    new BugNavigationBehavior(oreTargetOffsetX, oreTargetOffsetY, bugNavigationState).execute(rc);
    rc.setIndicatorString(1, roundNum + ": Ore far away, navigating to offset ("
        + oreTargetOffsetX + ", " + oreTargetOffsetY + ").");
  }

  private MapLocation computeOreTarget(RobotController rc) throws GameActionException {
    // Search in concentric circles for a tile with lots of ore and bug nav
    // towards it.
    MapLocation myLoc = rc.getLocation();
    for (int distance = 2; distance < MAX_SEARCH_DISTANCE; distance++) {
      for (int i = 0; i < 2 * distance; i++) {
        MapLocation target = myLoc.add(-distance + i, -distance);
        if (isValidOreTarget(target, rc)) {
          return target;
        }
      }
      for (int i = 0; i < 2 * distance; i++) {
        MapLocation target = myLoc.add(distance, -distance + i);
        if (isValidOreTarget(target, rc)) {
          return target;
        }
      }
      for (int i = 0; i < 2 * distance; i++) {
        MapLocation target = myLoc.add(distance - i, distance);
        if (isValidOreTarget(target, rc)) {
          return target;
        }
      }
      for (int i = 0; i < 2 * distance; i++) {
        MapLocation target = myLoc.add(-distance, distance - i);
        if (isValidOreTarget(target, rc)) {
          return target;
        }
      }
    }

    return null;
  }

  private boolean isValidOreTarget(MapLocation loc, RobotController rc) throws GameActionException {
    return rc.canSenseLocation(loc) && rc.senseRobotAtLocation(loc) == null &&
        rc.senseOre(loc) > MIN_ORE_TO_MINE;
  }

}
