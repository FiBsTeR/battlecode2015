package team102.behaviors;

import team102.Behavior;
import team102.BehaviorMessenger;
import team102.BehaviorType;
import team102.BehaviorUtils;
import team102.BugNavigationState;
import battlecode.common.DependencyProgress;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.TerrainTile;

public class MarioKartBehavior implements Behavior {

  private static final int MAX_DIST_FROM_TARGET = 50;

  private final BehaviorMessenger behaviorMessenger;
  private final BugNavigationState bugNavigationState;

  public MarioKartBehavior(
      BehaviorMessenger behaviorMessenger, BugNavigationState bugNavigationState) {
    this.behaviorMessenger = behaviorMessenger;
    this.bugNavigationState = bugNavigationState;
  }

  @Override
  public BehaviorType getBehaviorType() {
    return BehaviorType.MARIO_KART;
  }

  @Override
  public String getDescription() {
    return "Mario Kart!";
  }

  @Override
  public void execute(RobotController rc) throws GameActionException {
    if (!rc.isCoreReady()) {
      return;
    }
    if (maybeBuildHelipad(rc)) {
      return;
    }
    if (maybeBuildAerospaceLab(rc)) {
      return;
    }
    MapLocation enemyHq = rc.senseEnemyHQLocation();
    MapLocation myHq = rc.senseHQLocation();

    int offsetX = enemyHq.x - myHq.x;
    int offsetY = enemyHq.y - myHq.y;
    new BugNavigationBehavior(offsetX, offsetY, bugNavigationState).execute(rc);
  }

  private boolean maybeBuildHelipad(RobotController rc) throws GameActionException {
    return behaviorMessenger.getNumAlliedRobots(RobotType.HELIPAD) < 1 &&
        rc.getTeamOre() > RobotType.HELIPAD.oreCost &&
        maybeBuild(rc, RobotType.HELIPAD);
  }

  private boolean maybeBuildAerospaceLab(RobotController rc) throws GameActionException {
    return shouldBuildAerospaceLab(rc) &&
        rc.getTeamOre() > RobotType.AEROSPACELAB.oreCost &&
        rc.checkDependencyProgress(RobotType.HELIPAD) == DependencyProgress.DONE &&
        maybeBuild(rc, RobotType.AEROSPACELAB);
  }

  private boolean maybeBuild(RobotController rc, RobotType buildType) throws GameActionException {
    for (Direction d : Direction.values()) {
      if (d == Direction.NONE || d == Direction.OMNI) {
        continue;
      }
      MapLocation neighbor = rc.getLocation().add(d);
      if (shouldBuildAtLoc(rc, neighbor)) {
        rc.build(d, buildType);
        return true;
      }
    }
    return false;
  }

  private boolean shouldBuildAtLoc(RobotController rc, MapLocation loc) throws GameActionException {
    RobotInfo[] nearbyEnemyRobotInfos = new RobotInfo[] {};
    MapLocation enemyHqLoc = rc.senseEnemyHQLocation();
    MapLocation[] enemyTowerLocs = rc.senseEnemyTowerLocations();
    if (!isFreeLoc(rc, loc) ||
        !BehaviorUtils.isSafeLocation(loc, nearbyEnemyRobotInfos, enemyHqLoc, enemyTowerLocs)) {
      return false;
    }

    boolean freeNorth = isFreeLoc(rc, loc.add(Direction.NORTH));
    boolean freeEast = isFreeLoc(rc, loc.add(Direction.EAST));
    boolean freeSouth = isFreeLoc(rc, loc.add(Direction.SOUTH));
    boolean freeWest = isFreeLoc(rc, loc.add(Direction.WEST));
    return (freeEast || freeWest) && (freeNorth || freeSouth);
  }

  private boolean isFreeLoc(RobotController rc, MapLocation loc) throws GameActionException {
    return rc.senseRobotAtLocation(loc) == null &&
        rc.senseTerrainTile(loc) == TerrainTile.NORMAL;
  }

  private boolean shouldBuildAerospaceLab(RobotController rc) {
    MapLocation myLoc = rc.getLocation();

    MapLocation[] enemyTowerLocs = rc.senseEnemyTowerLocations();
    RobotInfo[] alliedRobots = rc.senseNearbyRobots(99999, rc.getTeam());
    MapLocation[] aerospaceLabLocs = new MapLocation[20];
    int numAerospaceLabs = 0;
    for (RobotInfo info : alliedRobots) {
      if (info.type == RobotType.AEROSPACELAB) {
        aerospaceLabLocs[numAerospaceLabs++] = info.location;
      }
    }

    MapLocation target = rc.senseEnemyHQLocation();
    int targetDist = myLoc.distanceSquaredTo(target);
    for (MapLocation enemyTowerLoc : enemyTowerLocs) {
      int dist = myLoc.distanceSquaredTo(enemyTowerLoc);
      if (dist < targetDist) {
        target = enemyTowerLoc;
        targetDist = dist;
      }
    }

    if (target == null || targetDist > MAX_DIST_FROM_TARGET) {
      return false;
    }

    for (int i = 0; i < numAerospaceLabs; i++) {
      MapLocation aerospaceLabLoc = aerospaceLabLocs[i];
      if (target.distanceSquaredTo(aerospaceLabLoc) < MAX_DIST_FROM_TARGET) {
        return false;
      }
    }

    return true;
  }

}
