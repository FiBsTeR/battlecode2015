package team102.behaviors;

import team102.Behavior;
import team102.BehaviorType;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class SpawnBehavior implements Behavior {

  private final RobotType spawnType;

  public SpawnBehavior(RobotType spawnType) {
    this.spawnType = spawnType;
  }

  public RobotType getSpawnType() {
    return spawnType;
  }

  @Override
  public BehaviorType getBehaviorType() {
    return BehaviorType.SPAWN;
  }

  @Override
  public String getDescription() {
    return "Spawn " + spawnType;
  }

  @Override
  public void execute(RobotController rc) throws GameActionException {
    if (!rc.isCoreReady() || rc.getTeamOre() < spawnType.oreCost) {
      return;
    }

    for (Direction d : Direction.values()) {
      if (rc.canSpawn(d, spawnType)) {
        rc.spawn(d, spawnType);
        return;
      }
    }
  }
}
