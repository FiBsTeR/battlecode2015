package team102;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;

public interface BuildOrder {

  public BehaviorCalculator getBehaviorCalculator(RobotController rc, RobotState robotState);

  public void initializeForRound(
      RobotController rc, BehaviorMessenger behaviorMessenger) throws GameActionException;

  public Behavior getBehaviorForRobot(RobotInfo robotInfo) throws GameActionException;
}
