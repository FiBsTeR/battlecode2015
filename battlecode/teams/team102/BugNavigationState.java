package team102;

import battlecode.common.Direction;


public class BugNavigationState {

  private boolean isTracing;
  private int traceDistance;
  private Direction nextStepDirection;
  private int stuckTurns;

  public BugNavigationState() {
    isTracing = false;
    traceDistance = 0;
  }

  public boolean isTracing() {
    return isTracing;
  }

  public int getTraceDistance() {
    return traceDistance;
  }

  public Direction getNextStepDirection() {
    return nextStepDirection;
  }

  public int getStuckTurns() {
    return stuckTurns;
  }

  public void startTrace(int traceDistance, Direction directionToWall) {
    this.isTracing = true;
    this.traceDistance = traceDistance;
    this.nextStepDirection = directionToWall;
    stuckTurns = 0;
  }

  public void trace(Direction step) {
    if (!isTracing) {
      return;
    }

    // Compute the next step direction via magic formula.
    nextStepDirection = Direction.values()[(step.ordinal() + 3) % 8];
    stuckTurns = 0;
  }

  public void stuck() {
    stuckTurns++;
  }

  public void endTrace() {
    isTracing = false;
    stuckTurns = 0;
  }

}
